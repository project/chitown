// SevenUp Plugin Example
// Author: Jonathan Howard
// Learn more at http://code.google.com/p/sevenup

// First step, rename this file so it's sevenup_<your_plugin_name>.<compatible.sevenup.version.number>.js
//Make sure sevenup was included, and fail gracefully if not.
if (sevenUp) {
  // "Register" the plugin with SevenUp so users can call our plugin if they like.
  // This is done by adding a Javascript object literal with your plugin name to 'sevenUp's 'plugin' property.
  // Also keeps global namespace to a min - still only one variable defined: sevenUp.
  sevenUp.plugin.plugin_name_here = {
    // Define the 'test' function users will call. You may want to define helper functions/vars here as well.
    // For continuity, test() should have the same function signature as sevenUp.test: "function test(options, callback)"
    // If you must add variables, look for them in newOptions instead of changing the function signature.
    test: function(newOptions, callback) {
      // User called your plugin instead of sevenUp directly- configure SevenUp before calling it.
      // Here we set options such that sevenUp will hide its default lightbox and use yours instead.
      newOptions.overrideLightbox = true;
      newOptions.lightboxHTML = "BEGIN YOUR CUSTOM HTML HERE";
      if (newOptions.enableQuitBuggingMe === false) {
        newOptions.lightboxHTML += "";  // Add link w/ onclick='sevenUp.close()'.
      } else {
        newOptions.lightboxHTML += "";  // Add link w/ onclick='sevenUp.quitBuggingMe()'.
      }
      newOptions.lightboxHTML += "END YOUR CUSTOM HTML HERE";
      // Options all set - call sevenUp.
      sevenUp.test(newOptions, callback);
    }
  };
}