<?php

/**
 * @file
 * Theme implementation to display a single Drupal page.
 *
 * @copyright (C) Copyright 2009 Palantir.net
 * @license http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $css: An array of CSS files for the current page.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $classes_array: Array of the body classes. It is flattened into a string
 *   within the variable $classes.
 * - $is_front: TRUE if the current page is the front page. Used to toggle the mission statement.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Page metadata:
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $head_title: A modified version of the page title, for use in the TITLE tag.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It should be placed within the <body> tag. When selecting through CSS
 *   it's recommended that you use the body tag, e.g., "body.front". It can be
 *   manipulated through the variable $classes_array from preprocess functions.
 *   The default values can be one or more of the following:
 *   - front: Page is the home page.
 *   - not-front: Page is not the home page.
 *   - logged-in: The current viewer is logged in.
 *   - not-logged-in: The current viewer is not logged in.
 *   - node-type-[node type]: When viewing a single node, the type of that node.
 *     For example, if the node is a "Blog entry" it would result in "node-type-blog".
 *     Note that the machine name will often be in a short form of the human readable label.
 *   The following only apply with the default 'sidebar_first' and 'sidebar_second' block regions:
 *     - two-sidebars: When both sidebars have content.
 *     - no-sidebars: When no sidebar content exists.
 *     - one-sidebar and sidebar-first or sidebar-second: A combination of the
 *       two classes when only one of the two sidebars have content.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $mission: The text of the site mission, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $search_box: HTML to display the search box, empty if search has been disabled.
 * - $primary_links (array): An array containing the Primary menu links for the
 *   site, if they have been configured.
 * - $secondary_links (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title: The page title, for use in the actual HTML content.
 * - $messages: HTML for status and error messages. Should be displayed prominently.
 * - $tabs: Tabs linking to any sub-pages beneath the current page (e.g., the view
 *   and edit tabs when displaying a node).
 * - $help: Dynamic help text, mostly for admin pages.
 * - $content: The main content of the current page.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $left: The HTML for the first sidebar.
 * - $right: The HTML for the second sidebar.
 *
 * Footer/closing data:
 * - $footer_message: The footer message as defined in the admin settings.
 * - $footer : The footer region.
 * - $closure: Final closing markup from any modules that have altered the page.
 *   This variable should always be output last, after all other dynamic content.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>">

  <div id="outerpage"><div id="outerpage-inner">

    <div id="login-bar"><div id="login-bar-inner" class="clearfix">
      <?php print $login_bar; ?>
      <?php if ($search_box): ?>
        <div id="search-box"><?php print $search_box; ?></div>
      <?php endif; ?>
    </div></div> <!-- /#login-bar-inner, /#login-bar -->

    <div id="header"><div id="header-position"> <div id="header-inner" class="clearfix">

      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
      <?php endif; ?>

      <div id="header-main" class="clear-fix">
        <?php if ($site_name || $site_slogan): ?>
          <div id="name-and-slogan">
            <?php if ($site_name): ?>
              <?php if ($title): ?>
                <div id="site-name"><strong>
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
                  <?php print $site_name; ?>
                  </a>
                </strong></div>
              <?php else: /* Use h1 when the content title is empty */ ?>
                <h1 id="site-name">
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
                  <?php print $site_name; ?>
                  </a>
                </h1>
              <?php endif; ?>
            <?php endif; ?>

            <?php if ($site_slogan): ?>
              <div id="site-slogan"><?php print $site_slogan; ?></div>
            <?php endif; ?>
          </div> <!-- /#name-and-slogan -->
        <?php endif; ?>

        <?php print $header; ?>

      </div>

      <?php if ($primary_links || $navbar): ?>
        <div id="navbar">

          <a name="navigation" id="navigation"></a>

          <?php print theme('links', $primary_links, array('id' => 'main-menu', 'class' => 'links clearfix')); ?>

          <?php print $navbar; ?>

        </div> <!-- /#navbar -->
      <?php endif; ?>

      <?php if($secondary_links): ?>
        <div id="secondary-nav">
          <div id="secondary-nav-inner">
            <?php print theme('links', $secondary_links, array('id' => 'secondary-menu', 'class' => 'links clearfix')); ?>
          </div>
        </div>
      <?php endif; ?>

    </div></div></div> <!-- /#header-inner, /#header-position /#header -->


  <div id="page"><div id="page-inner">

    <?php if ($primary_links || $navbar): ?>
      <div id="skip-to-nav"><a href="#navigation"><?php print t('Skip to Navigation'); ?></a></div>
    <?php endif; ?>

    <div id="main"><div id="main-inner" class="clearfix<?php if ($primary_links || $navbar) { print ' with-navbar'; } ?>">

      <div id="content"><div id="content-inner">

        <?php if ($mission): ?>
          <div id="mission"><?php print $mission; ?></div>
        <?php endif; ?>

        <?php print $content_top; ?>

        <?php if ($title): ?>
          <div id="content-header">
            <h1 class="title"><?php print $title; ?></h1>
          </div> <!-- /#content-header -->
        <?php endif; ?>

        <div id="content-area" class="clear-fix"><div id="content-area-inner">
        <?php if ($breadcrumb || $tabs || $help || $messages): ?>
          <div id="content-area-header">
            <?php print $breadcrumb; ?>
            <?php print $messages; ?>
            <?php if ($tabs): ?>
              <div class="tabs"><?php print $tabs; ?></div>
            <?php endif; ?>
            <?php print $help; ?>
          </div> <!-- /#content-area-header -->
        <?php endif; ?>

          <?php print $content; ?>
        </div></div> <!-- /#content-area-inner, /#content-area -->

        <?php if ($feed_icons): ?>
          <div class="feed-icons"><?php print $feed_icons; ?></div>
        <?php endif; ?>

        <?php print $content_bottom; ?>

      </div></div> <!-- /#content-inner, /#content -->

    <?php if ($quickquote || $left || $right): ?>
        <div id="sidebars-wrapper"><div id="sidebars-wrapper-inner">
          <?php if ($quickquote): ?>
          <div id="quickquote"><div id="quickquote-inner" class="region region-quickquote">
            <?php print $quickquote; ?>
          </div></div> <!-- /#quickquote-inner, /#quickquote -->
        <?php endif; ?>

        <?php if ($left): ?>
          <div id="sidebar-first"><div id="sidebar-first-inner" class="region sidebar region-sidebar-first">
            <?php print $left; ?>
          </div></div> <!-- /#sidebar-first-inner, /#sidebar-first -->
        <?php endif; ?>

        <?php if ($right): ?>
          <div id="sidebar-second"><div id="sidebar-second-inner" class="region sidebar region-sidebar-second">
            <?php print $right; ?>
          </div></div> <!-- /#sidebar-second-inner, /#sidebar-second -->
        <?php endif; ?>
      </div></div>
   <?php endif; ?>

    </div></div> <!-- /#main-inner, /#main -->

  </div></div> <!-- /#page-inner, /#page -->

</div></div> <!-- /#outerpage-inner, /#outerpage -->

    <?php if ($footer || $footer_message || $secondary_links || $footer_bottom): ?>
      <div id="footer"><div id="footer-inner" class="region region-footer">

        <div id="footer-top"><div id="footer-top-inner">
          <div class="footer-position">
            <?php print $footer; ?>
          </div> <!-- /#footer-position -->
        </div></div> <!-- /#footer-top-inner, /#footer-top -->

        <div id="footer-bottom"><div id="footer-bottom-inner">
          <div class="footer-position">

            <?php if ($footer_message): ?>
              <div id="footer-message"><?php print $footer_message; ?></div>
            <?php endif; ?>
            <?php print $footer_bottom; ?>

          </div> <!-- /#footer-position -->
        </div></div> <!-- /#footer-bottom-inner, /#footer-bottom -->

    </div></div> <!-- /#footer-inner, /#footer -->
    <?php endif; ?>

  <?php print $closure_region; ?>

  <?php print $closure; ?>

</body>
</html>
