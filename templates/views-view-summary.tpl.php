<?php
/**
 * @file views-view-summary.tpl.php
 * Default simple view template to display a list of summary lines
 *
 * @ingroup views_templates
 */


  $rowCountTotal = 1;
  $count = 1;
  
  foreach ($rows as $row) {
    $rowCountTotal = ++$rowCountTotal;
  }
?>
<div class="item-list">
  <ul class="views-summary">
  <?php foreach ($rows as $row): ?>
    <?php if ($count == 1): ?>
      <li class="views-row views-row-1 views-row-first">
    <?php elseif ($count == $rowCountTotal-1): ?>
      <li class="views-row views-row-<?php print $count ?> views-row-last">
    <?php else: ?>
      <li class="views-row views-row-<?php print $count ?>">
    <?php endif; ?>
      <a href="<?php print $row->url; ?>"><?php print $row->link; ?></a>
      <?php if (!empty($options['count'])): ?>
        (<?php print $row->count?>)
      <?php endif; 
   ++$count;   
      ?>
    </li>
  <?php endforeach; ?>
  </ul>
</div>
