<?php

/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout, with
 * additional areas for the top and the bottom.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['top']: Content in the top row.
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 *   - $content['bottom']: Content in the bottom row.
 */
?>
<div class="panel-twocolumnonerow clear-block panel-display" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="panel-center-wrapper">
    <div class="panel-col-first panel-panel">
      <div class="inside"><?php print $content['firstcolumn']; ?></div>
    </div> <!-- /.panel-col-first -->

    <div class="panel-col-last panel-panel">
      <div class="panel-col-last-top panel-panel">
        <div class="inside"><?php print $content['secondcolumntop']; ?></div>
      </div><!-- /.panel-col-last-top -->
     <div class="panel-col-last-bottom">
       <div class="panel-col-last-left panel-panel">
         <div class="inside"><?php print $content['secondcolumnleft']; ?></div>
       </div>
       <div class="panel-col-last-right panel-panel">
         <div class="inside"><?php print $content['secondcolumnright']; ?></div>
       </div>     
    </div><!-- /.panel-col-last-bottom -->
    </div> <!-- /.panel-col-last -->

  </div> <!-- /.panel-center-wrapper -->
  
  <div class="panel-col-bottom panel-panel">
    <div class="inside"><?php print $content['bottom']; ?></div>
  </div> <!-- /.panel-col-bottom -->
</div> <!-- /.panel-twocolumnonerow -->
