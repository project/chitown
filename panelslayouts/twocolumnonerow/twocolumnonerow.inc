<?php

/**
 * Implementation of hook_panels_layouts().
 */
function chitown_twocolumnonerow_panels_layouts() {
  $items['twocolumnonerow'] = array(
    'title' => t('Chitown 2 col 1 row'),
    'icon' => 'twocolumnonerow.png',
    'theme' => 'panels_twocolumnonerow',
    'css' => 'twocolumnonerow.css',
    'panels' => array(
      'firstcolumn' => t('First column'),
      'secondcolumntop' => t('Second column top'),
      'secondcolumnleft' => t('Second column left'),
      'secondcolumnright' => t('Second column right'),
      'bottom' => t('Bottom')
    ),
  );

  return $items;
}
