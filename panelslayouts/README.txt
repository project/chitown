These panels layouts were created by Palantir.net for the Chitown theme.  This theme was initially built for Drupal Camp Chicago 2009, but is intended to be used by many other sites. To use this on your own site follow the documentation provided by Panels here: http://drupal.org/node/495654.

*Please note: These panels layouts will not show up if you're using a different theme for your administration theme.
